import sys
from setuptools import setup, find_packages

# We're python3-only
if sys.version_info < (3, 4):
    raise RuntimeError('getmusic requires Python 3.4 or later to run.')

setup(
    name = "getmusic",
    version = "0.1a0",
    description = "Download and organize Creative Commons-licensed music.",
    packages = find_packages(),
    install_requires = ['requests'],
    license = "GPLv3",
    author = "Yuriy Bulka",
    author_email = "setthemfree@privacyrequired.com",
    entry_points = {
        'console_scripts': [
            'getmusic = getmusic:main'
        ]
    },
    setup_requires = ['pytest-runner'],
    tests_require = ['pytest', 'pytest-console-scripts']
)
