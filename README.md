getmusic
========

Download and organize Creative Commons-licensed music.

**Warning:** This is still in an early stage of development, so be
prepared to report bugs:)

Requirements
============

`getmusic` is written for Python 3.4 or later and has only been tested
on Gnu/Linux. Additionally, it uses the following packages:

-   [requests](http://docs.python-requests.org/en/master/)

Installation
============

Please use `python3 setup.py install` to install the package. Do this
inside a python3 virtualenv if you don't want to install system-wide.

Usage
=====

To download a music release from a supported provider, just run:

    getmusic URL

This will download the release at `URL` to a subdirectory of your music
directory as specified in the configuration file. You may alternatively
run `getmusic --list-media URL` to only print the URLs of media files to
stdout without downloading (useful for piping into a player).

Supported providers
===================

Currently `getmusic` supports downloading creative commons-licensed
music from the following sources:

-   [Archive.org's netlabels collection](https://archive.org/netlabels)
-   [kahvi.org](http://kahvi.org)

Configuration
=============

`getmusic` will look for configuration file in `~/config/getmusic.conf`
by default (can be overridden with `-c` command line option). Please see
`getmusic-sample.conf` for an example configuration file with default
values. The configuration file is in the ini format and should start
with section `[general]`. The following configuration options are
supported:

-   `music_dir`: path to the "root" of your music library. `~` will be
    replaced with your home directory.
-   `album_path`: the directory structure for the downloaded music under
    your `music_dir`. Supports the following placeholders that will be
    replaced by values from the metadata about the downloaded release:
    -   `{artist}`: the release artist
    -   `{title}`: the release title; this placeholder must be present
        in `album_path`
    -   `{collection}`: for archive.org this means which netlabel the
        release was published on, for other providers either the
        provider name or whatever notion of a collection/category the
        provider might have.
-   `field_unknown`: if a metadata field is not defined for a release,
    it will be substituted with the value of this configuration option.
-   `lossless`: if enabled (`on`, `True`, `1`), prefer lossless formats
    over lossy ones (i.e. `flac` over `ogg` for instance). Can be
    overridden with `--lossless` or `--lossy` command line switch.
-   `formats_lossy`: a comma-separated list of desired formats to
    download if `lossless` is off; media will be downloaded in the first
    format of these that is available. Can be overridden with
    `--formats-lossy` command line option.
-   `formats_lossless`: a comma-separated list of desired formats to
    download if `lossless` is on; media will be downloaded in the first
    format of these that is available. Can be overridden with
    `--formats-lossless` command line option.

Roadmap
=======

-   Sanitize fields used in `album_path` before writing to disk (duh)
-   Add Tor support (likely requires switching from requests to pycurl)
-   Fix `pylint` warnings
-   Create progress bar.
-   Validate downloads with checksums.
-   Support continuing interrupted downloads.
-   Support more sources:
    -   Free Music Archive
    -   Jamendo
    -   ccmixter
    -   Wikimedia Commons
