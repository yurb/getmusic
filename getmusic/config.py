import configparser
import string
from os.path import expanduser
from collections import OrderedDict

DEFAULT_CONFIG_PATH = expanduser("~/.config/getmusic.conf")

DEFAULTS = OrderedDict([
    ('music_dir', '~/Music'),
    ('album_path', '{collection}/{artist}/{title}'),
    ('field_unknown', 'Unknown'),
    ('lossless', 'False'),
    ('formats_lossy', 'ogg,mp3'),
    ('formats_lossless', 'flac,wav,aiff')
])

#: Which placeholders in 'album_path' configuration option are supported
SUPPORTED_PATH_PLACEHOLDERS = (
    'artist',
    'title',
    'collection'
)

#: Which placeholders in 'album_path' configuration option are required
REQUIRED_PATH_PLACEHOLDERS = (
    'title',
)

# Which formats we understand
LOSSY = ('ogg', 'mp3')
LOSSLESS = ('flac', 'wav', 'aiff')

# Exceptions for testing
class MissingPathPlaceholderError(ValueError):
    pass

class UnsupportedPathPlaceholderError(ValueError):
    pass

def init_config():
    """Return a ``ConfigParser`` object with getmusic's settings."""
    return configparser.ConfigParser(defaults=DEFAULTS,
                                     default_section="general")

def load_config(path, required=False):
    """Load configuration from a file and return it as a plain dict.

    :param required: if True, raise ValueError if file cannot be read.
        Otherwise fall back to defaults.
    """
    config_parser = init_config()

    try:
        if required:
            with open(path) as opened:
                config_parser.read_file(opened)
        else:
            config_parser.read(path)
    except configparser.MissingSectionHeaderError:
        raise ValueError(
            "The configuration file doesn't contain any section headers. "
            "Please define at least one section (for instance 'general').")
    except OSError as error:
        raise ValueError(
            'The configuration file "{}" could not be read: {}'
            .format(path, error))

    # We'll just store the configuration in memory as a regular dict,
    # because we don't need the configparser's strict requirement that
    # everything should be strings
    config = dict(config_parser['general'])
    config['lossless'] = config_parser['general'].getboolean('lossless')

    # Expand special characters in values
    config['music_dir'] = expanduser(config['music_dir'])

    # Validate values
    formatter = string.Formatter()
    album_path_components = [i[1] for i in
                             list(formatter.parse(
                                 config['album_path']))]

    for placeholder in REQUIRED_PATH_PLACEHOLDERS:
        if placeholder not in album_path_components:
            raise MissingPathPlaceholderError(
                'The "{}" placeholder is missing in the '
                '"album_path" configuration option. Please see the README.'
                .format(placeholder))

    for placeholder in album_path_components:
        if placeholder not in SUPPORTED_PATH_PLACEHOLDERS:
            raise UnsupportedPathPlaceholderError(
                'The "{}" placeholder in "album_path" configuration is not '
                'supported. Please see the README.'
                .format(placeholder))

    return config

def prioritize_formats(config):
    """Build and return a single list of formats that should be tried
    based on the `formats-lossy`, `formats-lossless` and `lossless`
    configuration/command line options.
    """
    lossy = [x.strip() for x in config['formats_lossy'].split(',')]
    for fmt in lossy:
        if fmt not in LOSSY:
            raise ValueError("Unknown lossy format specified: {}".format(fmt))

    lossless = [x.strip() for x in config['formats_lossless'].split(',')]
    for fmt in lossless:
        if fmt not in LOSSLESS:
            raise ValueError("Unknown lossless format specified: {}".format(fmt))

    if config['lossless']:
        formats = lossless + lossy
    else:
        formats = lossy + lossless

    return formats
