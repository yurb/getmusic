"""Logic related to storing files on disk."""

import os

from collections import namedtuple, defaultdict

from .util import unique

# Each asset is something we wish to download
Asset = namedtuple('Asset', [
    'title', # Track title
    'filename', # Destination file name
    'url', # Source URL
    'fmt', # One of the formats in LOSSY, LOSSLESS (defined in the
           # config module), or the string 'other'
])

class Release:
    """Represents a release (album, single, etc.) that the user might
    download. Has the following instance variables, passed as
    parameters to its constructor:

    :param metadata: A dictionary representing the metadata about the
      release. The following fields are expected, although if missing,
      will be substituted by the value of the `field_missing`
      configuration option:

      - artist

      - title

      - collection: if the provider has some notion of a collection,
        or the provider's name otherwise.

    :param assets: a list of :any:`Asset` namedtuples defining each
      file that is available for download.

    """
    def __init__(self, metadata, assets):
        self.metadata = metadata
        self.assets = assets

    def filter_assets(self, formats):
        """Return a filtered list of assets where each media is present only
        once in the desired format.

        :param formats: a list of formats as returned by
          :func:`~getmusic.config.prioritize_formats`.
        """
        # Build a list of unique media titles in the release
        titles = unique([asset.title for asset in self.assets
                         if not asset.fmt == 'other'])

        # Iterate over all assets
        media = [self._find_best_media(title, formats) for title in titles]
        non_media = [asset for asset in self.assets
                     if asset.fmt == 'other']

        return media + non_media

    def _find_best_media(self, title, formats):
        """For an asset title, find and return the asset that is in a
        most suitable format."""
        # FIXME: currently doesn't know anything about different
        # bitrates or quality profiles of the same format; returns the
        # first available asset in the given format. Some kind of
        # representation of bitrate or quality is needed at the level
        # of Asset.
        for fmt in formats:
            matching = next((asset for asset in self.assets
                             if asset.title == title
                             and asset.fmt == fmt), None)
            if matching:
                return matching

        raise RuntimeError("Media '{}' is not available in any of the requested formats. "
                           "The requested formats include, in that order: {}."
                           .format(title, ", ".join(formats)))

#: Characters that are forbidden in a filename, and their replacements
FILENAME_REPLACE = {
    '/': '&',
    '\\': '',
    '?': '',
    '!': '',
    '%': '_',
    '*': '_',
    ':': '-',
    '|': '_',
    '"': '',
    '\'': '',
    '<': '',
    '>': '',
    '[': '',
    ']': '',
}

def _safe_char(char):
    """Replace unsafe filename character with a safe one."""
    if char in FILENAME_REPLACE.keys():
        return FILENAME_REPLACE[char]
    return char

def safe_filename(filename):
    """Replace unsafe characters from filename with safe ones."""
    safe = [_safe_char(char) for char
            in filename]
    return "".join(safe)

class MusicLibrary:
    """This class is responsible for deciding where each downloaded asset
    should be stored on the filesystem and how it should be named."""

    def __init__(self, config):
        self.config = config

    def _get_album_path(self, metadata):
        """Return the path of a directory that the album's files should be
        downloaded to, relative to music directory."""
        # If a field is missing in metadata, substitute by the value
        # of 'field_unknown' configuration option
        meta = defaultdict(lambda: self.config['field_unknown'], metadata)

        # Sanitize the fields
        meta_safe = {k: safe_filename(v) for k, v in meta.items()}

        album_path_format = self.config['album_path']
        album_path = album_path_format.format(**meta_safe)

        return album_path

    def open(self, asset, metadata):
        """Return an opened file for an asset to be downloaded to.

        :param asset: The Asset namedtuple describing the file.
        :param metadata: Metadata about the release.
        """
        # Create target directory (if needed)
        dest_dir = os.path.join(self.config['music_dir'],
                                self._get_album_path(metadata))
        try:
            os.makedirs(dest_dir, exist_ok=True)
        except OSError as error:
            raise RuntimeError('Could not prepare target directory: %s', error)

        file_path = os.path.join(dest_dir, safe_filename(asset.filename))
        mode = 'wb' if self.config['overwrite'] else 'xb'
        return open(file_path, mode)
