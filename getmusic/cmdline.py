import argparse

LOGLEVELS = ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')

def config_on_cmdline(argv):
    """Returns configuration file path specified on command line, if any.

    :param argv: The program argument list.
    """
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-c", "--config")

    partial, _ = parser.parse_known_args(argv[1:])
    return partial.config

def parse_arguments(argv, config):
    """Parse all command line with argparse and return the result.

    :param argv: The program argument list.

    :param config: The global config based on defaults and configuration file.
    """
    parser = argparse.ArgumentParser(
        description="Download freely-licensed music from known sources.")

    # Arguments
    parser.add_argument("url", metavar="URL",
                        help="URL of a web page with album or track to download")

    # Options
    parser.add_argument("-c", "--config",
                        help="Specify alternative configuration file to use.")
    parser.add_argument("-y", dest='overwrite',
                        help="Allow overwriting existing files.",
                        action='store_true')
    parser.add_argument("-v", "--loglevel",
                        help="Set verbosity level. Defaults to INFO.",
                        choices=LOGLEVELS,
                        default="INFO")
    parser.add_argument("-l", "--list-media",
                        help="Print URLs of media files to stdout, but don't download anything"
                        " (useful for piping to a player for example).",
                        action='store_true')
    parser.add_argument("--formats-lossy",
                        help="Comma-separated list of desired lossy formats. "
                        "Media will be downloaded in the first format in this list "
                        "that is available and --lossy is on.",
                        default=config['formats_lossy'])
    parser.add_argument("--formats-lossless",
                        help="Comma-separated list of desired lossless formats. "
                        "Media will be downloaded in the first format in this list "
                        "that is available and --lossless is on.",
                        default=config['formats_lossless'])

    lossless_or_lossy = parser.add_mutually_exclusive_group()
    lossless_or_lossy.add_argument("--lossless",
                                   help="Download in lossless format if available"
                                   + (" (default)" if config['lossless'] else ""),
                                   default=config['lossless'],
                                   action='store_true')
    lossless_or_lossy.add_argument("--lossy",
                                   help="Download in lossy format if available"
                                   + (" (default)" if not config['lossless'] else ""),
                                   default=config['lossless'],
                                   action='store_false',
                                   dest='lossless')

    return parser.parse_args(argv[1:])
