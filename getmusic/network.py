"""A library-agnostic interface to network IO."""

import logging

import requests

#: The chunk size for iterating over downloaded content
CHUNK_SIZE = 1024 ** 2

# Make requests less verbose
logging.getLogger('requests').setLevel(logging.WARNING)

def get(url):
    """Perform a GET request and return the response as a string.

    If an error or an unsatisfatory response code (like 404) is
    returned, raise an appropriate exception.

    :param url: the target URL
    """
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.RequestException as error:
        raise RuntimeError(error)

    return response.text

def get_to_file(url, dest):
    """Perform a GET request and write the result to a file.

    :param url: the target URL
    :param dest: an opened file-like object to write to
    """
    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()
    except requests.exceptions.RequestException as error:
        raise RuntimeError(error)
    for chunk in response.iter_content(CHUNK_SIZE):
        dest.write(chunk)
