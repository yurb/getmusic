"""
Download and organize Creative Commons-licensed music.
"""
import sys
import logging

from .config import DEFAULT_CONFIG_PATH, load_config, prioritize_formats
from .cmdline import parse_arguments, config_on_cmdline
from .downloader import download, list_media
from .providers import provider_for_url

def main():
    # We parse the command line in two stages. First, we get the value
    # of the configuration file path (-c), if specified, and parse the
    # config file. We need to do this, because other command line
    # options defaults depend on the values in the config file. Then,
    # we parse the rest of the command line.
    custom_config_path = config_on_cmdline(sys.argv)
    try:
        if custom_config_path:
            config = load_config(custom_config_path, required=True)
        else:
            config = load_config(DEFAULT_CONFIG_PATH)
    except (RuntimeError, ValueError) as error:
        logging.critical("Could not parse configuration file: %s", error)
        return 1

    # Parse the rest of the command line
    args = parse_arguments(sys.argv, config)

    # Setup logging
    # (The args.loglevel should have already been validated by argparse)
    loglevel_numeric = getattr(logging, args.loglevel)
    logging.basicConfig(level=loglevel_numeric,
                        format='%(levelname)s: %(message)s')

    # Integrate some command line options into config
    config['overwrite'] = args.overwrite
    config['lossless'] = args.lossless
    config['formats_lossy'] = args.formats_lossy
    config['formats_lossless'] = args.formats_lossless
    config['preferred_formats'] = prioritize_formats(config)

    # Download
    try:
        provider = provider_for_url(args.url)
        if args.list_media:
            urls = list_media(args.url, provider, config['preferred_formats'])
            for url in urls:
                print(url)
        else:
            download(args.url, provider, config)

    except (RuntimeError, ValueError) as error:
        logging.critical("Error processing item: %s", error)
        return 1
