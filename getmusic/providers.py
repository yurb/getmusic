"""Providers represent the websites from where the user can download
Creative Commons-licensed music.

Each provider class should provide (for now) the following method:

 - :func:`get_release`: provided with a URL, should return a
   :class:`~getmusic.musiclib.Release` instance located at url. See
   the class documentation for a description of its attributes.

The supported URL regexps are mapped to providers in the
`PROVIDERS_MAP` module constant.
"""
import os
import re
import json
import xml.etree.ElementTree as ET

from urllib.parse import urlparse, quote
from operator import itemgetter

import getmusic.network as network
from .musiclib import Release, Asset
from .util import error

def provider_for_url(url):
    """Return an instance of a matching provider class for a url."""
    for regexp, provider in PROVIDERS_MAP.items():
        if re.search(regexp, url):
            # Found corresponding class
            matching = provider()
            break

    else: # No matching class found
        raise ValueError("Downloading from {} is not supported yet.".format(url))

    return matching

class ArchiveOrgProvider(object):
    """archive.org."""

    #: Which files to skip from download
    SKIP_FILES = r"(.*\.zip|.*\.afpk|.*\.json\.gz|.*_spectrogram\.png|.*thumbnail.*)"
    #: Archive.org's URL for retrieving metadata about an item
    INFO_URL_FORMAT = '{scheme}://archive.org/metadata/{item}'
    #: Archive.org's URL for downloading individual assets
    DOWNLOAD_URL_FORMAT = '{scheme}://archive.org/download/{item}/{asset}'
    #: Suffixes that Archive.org appends to files when converting to different bitrates
    BITRATE_SUFFIXES = r'(_vbr|_64kb)$'

    #: Which of Archive.org's formats mean what
    FORMATS = {
        '.*MP3': 'mp3',
        'Ogg Vorbis': 'ogg',
        '.*Flac': 'flac',
        'AIFF': 'aiff',
        'WAVE': 'wav',
    }

    def __init__(self):
        self.scheme = 'https'
        self.url = None

    def get_release(self, url):
        self.url = url
        url_p = urlparse(url)
        self.scheme = url_p.scheme

        item_id = self._get_item_id(url_p)
        release_info = self._fetch_release_info(item_id)

        return Release(
            self._metadata(release_info),
            self._assets(release_info, item_id)
        )

    def _metadata(self, release_info):
        """Return a dict with metadata about the album."""
        mapping = {
            # archive.org: ours
            'title': 'title',
            'creator': 'artist',
            'collection': 'collection'
        }

        # Copy all items that ain't missing
        meta = {}
        for src, dest in mapping.items():
            try:
                meta[dest] = release_info['metadata'][src]
            except KeyError:
                continue

        # Check if the item is in multiple collections, and if so, use the first one
        # TODO: Can this be done without using isinstance (i.e. do duck typing)?
        if isinstance(meta['collection'], list):
            meta['collection'] = meta['collection'][0]

        return meta

    def _assets(self, release_info, item_id):
        """Return list of :class:`Asset` objects describing each file."""
        files = release_info['files']

        # Order by track number, then alphabetically
        files.sort(key=itemgetter('name'))
        files.sort(key=self._track_sorting)

        blacklist_re = re.compile(self.SKIP_FILES)

        return [self._as_asset(element, item_id) for element in files
                if not blacklist_re.match(element['name'])]

    def _track_sorting(self, file):
        """Return a sort key for an element in the 'files' json element, based
        on the track number. If a file has no track number, a sort key
        of 9999 is returned.
        """
        try:
            track = file['track'].split('/')[0]
            return int(track)
        except KeyError:
            return 9999

    def _fetch_release_info(self, item_id):
        info_url = self._get_info_url(item_id)
        with error("Failed to fetch metadata"):
            info = network.get(info_url)
            info = json.loads(info)
            return info

    def _get_item_id(self, url_p):
        """Return an Archive.org item ID based on item url in self._url_parsed."""
        if re.search(r'/(details|download|compress)', url_p.path):
            # The second component in the URL's path is the item ID
            item = url_p.path.split("/")[2]
            return item
        else:
            raise ValueError("The URL '{}' is not supported.".format(self.url))

    def _get_info_url(self, item_id):
        """Return the URL of the json description for the release."""
        # Build the new url
        info_url = self.INFO_URL_FORMAT.format(scheme=self.scheme,
                                               item=item_id)
        return info_url

    def _guess_track_title(self, filename):
        """Based on a file name, return the track title.

        The track title is something that is the same for all of
        the files that represent the same track independent of its
        format or bitrate.

        We decided not to rely on the `title` field in
        archive.org's metadata, because it was inconsistent,
        unfortunately.
        """
        # First, remove the extention
        title, _ = os.path.splitext(filename)

        # Second, remove any format/bitrate-dependent suffixes
        title = re.sub(self.BITRATE_SUFFIXES, '', title)

        return title

    def _which_format(self, aorg_fmt):
        """Return a format string based on aorg's format specification."""
        fmt = next((value for key, value in self.FORMATS.items()
                    if re.match(key, aorg_fmt)),
                   'other')
        return fmt

    def _as_asset(self, element, item_id):
        """Build an Asset object from archive.org json element."""
        filename = element['name']
        title = self._guess_track_title(filename)
        url = self.DOWNLOAD_URL_FORMAT.format(
            scheme=self.scheme,
            item=item_id,
            asset=quote(element['name'], safe=''))
        fmt = self._which_format(element['format'])

        return Asset(title, filename, url, fmt)


class KahviProvider(object):
    """Provider for kahvi.org"""
    #: The regexp of the release id in the URL
    RELEASE_ID_RE = r".*\?release_number=(\d+)"
    #: The regexp of the release artist and title on the release page
    TITLE_RE = r"<title>#\d+: (.*?) / (.*?) - Kahvi Collective Electronic Vibes</title>"
    #: The URL scheme of a release page
    RELEASE_PAGE_URL = "http://kahvi.org/releases.php?release_number={release_id}"
    #: The URL scheme of a playlist of tracks in a release
    PLAYLIST_PAGE_URL = "http://www.kahvi.org/playlist.php?release_number={release_id}"

    def get_release(self, url):
        release_id = self._get_release_id(url)
        return Release(
            self._metadata(release_id),
            self._assets(release_id)
        )

    def _metadata(self, release_id):
        # Fetch the release page
        with error("Failed to fetch the release page"):
            release_page = network.get(self.RELEASE_PAGE_URL
                                       .format(release_id=release_id))
        # Extract metadata
        metadata = self._extract_artist_title(release_page)

        # Kahvi doesn't have sub-collections
        metadata['collection'] = "kahvi"
        # Per http://kahvi.org/information.php
        metadata['license'] = "http://creativecommons.org/licenses/by-nc-nd/3.0/"

        return metadata

    def _assets(self, release_id):
        with error("Failed to fetch playlist"):
            playlist = network.get(self.PLAYLIST_PAGE_URL
                                   .format(release_id=release_id))
        xml = ET.fromstring(playlist)
        assets = [self._as_asset(element) for element
                  in xml.iter('{http://xspf.org/ns/0/}track')]
        return assets

    def _get_release_id(self, url):
        """Return kahvi release ID based on the URL."""
        match = re.match(self.RELEASE_ID_RE, url)
        if match:
            return match.group(1)
        else:
            raise ValueError("The URL '{}' is not supported.")

    def _extract_artist_title(self, page):
        """Extract the artist and the title from the release page.

        :param page: the html release page as a string.

        :returns: a dict with ``artist`` and ``title`` keys."""
        # We'll go with plain old regexps here
        match = re.search(self.TITLE_RE, page)
        if match:
            metadata = {'artist': match.group(1), 'title': match.group(2)}
            return metadata
        else:
            raise ValueError("Can't exctract info from release page title tag.")

    def _as_asset(self, element):
        """Build an cls:`Asset` from an xml element of a track in a playlist.

        :param element: an cls:`xml.etree.ElementTree.Element`
            representing the xml element of a track in an xpsf playlist.
        """
        title = element.find("{http://xspf.org/ns/0/}title").text
        url = element.find("{http://xspf.org/ns/0/}location").text
        fmt = 'mp3'
        url_parsed = urlparse(url)
        _, filename = os.path.split(url_parsed.path)
        return Asset(title, filename, url, fmt)

PROVIDERS_MAP = {
    # URL regexp: Class name
    r'^http(s?)://(www\.|)archive\.org': ArchiveOrgProvider,
    r'^http(s?)://(www\.|)kahvi\.org': KahviProvider,
}
