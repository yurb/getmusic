"""Various functions and constants."""

from contextlib import contextmanager

@contextmanager
def error(message, exception=RuntimeError):
    """A context manager to reraise exceptions with a descriptive message.

    It will run the code in a try/except block, and if an exception
    happens it will reraise it and prepend ``message`` to the
    exception's value in the format ``message: original value``.

    This is a syntactic sugar to write::

        with error("Can't fetch page"):
           network.get("https://some.url/page.html")

    Instead of::

        try:
            network.get("https://some.url/page.html")
        except RuntimeError as e:
            raise RuntimeError("Can't fetch page: " + str(e))

    :param message: The message to prepend to the reraised exception's value.

    :param exception: An optional parameter to specify the exception
        type (default: `RuntimeError`)
    """
    try:
        yield
    except exception as e:
        raise exception(message + ": " + str(e))

def unique(collection):
    """Return a list containing each element from collection only once,
    preserving order."""
    # https://stackoverflow.com/a/480227
    seen = set()
    return [x for x in collection
            if not (x in seen or seen.add(x))]
