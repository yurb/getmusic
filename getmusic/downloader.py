"""Manages the process of selecting and downloading media.

The main entry point is :func:`download()`
"""
import logging

import getmusic.network as network

from .musiclib import MusicLibrary

# Logging
logger = logging.getLogger(__name__)

def download(url, provider, config):
    """Download a release in the preferred formats.

    :param url: A URL of the release to download.

    :param provider: An instance of a provider class for the
       corresponding online site.

    :param config: The program's configuration as returned by
        :func:`load_config()`.

    """
    # Get metadata and list of assets
    logger.info('Fetching metadata')
    release = provider.get_release(url)
    musiclib = MusicLibrary(config)

    # Download
    download_assets(release, musiclib, config['preferred_formats'])

def list_media(url, provider, formats):
    """Return a list of the URLs of media files in the release.

    :param url: A URL of the release to download.

    :param provider: An instance of a provider class for the
       corresponding online site.

    :param preferred_formats: A list of formats as returned by
        :func:`~getmusic.config.prioritize_formats()`
    """
    release = provider.get_release(url)
    assets = release.filter_assets(formats)
    urls = [asset.url for asset in assets if asset.fmt != 'other']

    return urls

def download_assets(release, musiclib, formats):
    """Download each asset to the destination directory.

    :param release: an instance of :class:`getmusic.musiclib.Release`.

    :param musiclib: an instance of :class:`getmusic.musiclib.MusicLibrary`.

    :param formats: a list of preferred formats to download as
      returned by :func:`~getmusic.config.prioritize_formats`.
    """
    skipped = False
    assets = release.filter_assets(formats)
    for i, asset in enumerate(assets):
        try:
            with musiclib.open(asset, release.metadata) as dest_file:
                logger.info('[%d/%d] Downloading %s...',
                            i+1, len(assets), asset.filename)
                network.get_to_file(asset.url, dest_file)
        except FileExistsError:
            skipped = True
            logging.warning("Skipping '%s'.",
                            asset.filename)
    if skipped:
        logging.warning(
            "Some files were skipped because they already exist. "
            "Use -y to overwrite files if needed.")
