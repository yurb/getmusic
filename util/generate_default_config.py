"""This script generates an example configuration file with default values."""

from os.path import dirname, abspath, basename
from getmusic.config import init_config

def generate_default_config(path):
    """Generate configuration file based on defaults specified in ``getmusic.config`` module.

    :param path: Path to the file to write the config to.
    """
    default_config = init_config()
    
    with open(path, 'w') as dest:
        dest.write("; This file was autogenerated by {}\n"
                   .format(basename(__file__)))
        default_config.write(dest)

if __name__ == '__main__':
    path = dirname(dirname(abspath(__file__))) + '/getmusic-sample.conf'
    generate_default_config(path)
