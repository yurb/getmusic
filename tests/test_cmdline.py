import pytest
import argparse
from getmusic.cmdline import config_on_cmdline, parse_arguments
from getmusic.config import DEFAULTS

def test_config_on_cmdline_none():
    """Test if it config_on_cmdline returns None if config not specified"""
    assert config_on_cmdline(["/usr/bin/self"]) == None

def test_config_on_cmdline():
    """Test if config_on_cmdline returns the path if specified"""
    argv = ["/usr/bin/self", "-c", "/home/user/custom-config.conf"]
    assert config_on_cmdline(argv) == "/home/user/custom-config.conf"

@pytest.mark.parametrize("arguments, argument, value", [
    ("https://some.url/test", "url", "https://some.url/test"),
    ("--list-media https://some.url/test", 'list_media', True),
    ("-v INFO https://some.url/test", "loglevel", "INFO"),
    ("--loglevel INFO https://some.url/test", "loglevel", "INFO"),
    ("--formats-lossy mp3,ogg https://some.url/test", "formats_lossy", "mp3,ogg"),
    ("--formats-lossless flac,wav https://some.url/test", "formats_lossless", "flac,wav"),
    ("--lossless https://some.url/test", "lossless", True),
    ("--lossy https://some.url/test", "lossless", False),
    ("-y https://some.url/test", "overwrite", True),
    ("https://some.url/test", "overwrite", False),
])
def test_parse_arguments(arguments, argument, value):
    "Ensure the command line parameters correctly set confguration values."""
    parsed = parse_arguments([""] + arguments.split(), DEFAULTS)
    assert getattr(parsed, argument) == value

@pytest.mark.parametrize("option, value, help_text", [
    ("lossless", True, "Download in lossless format if available (default)"),
    ("lossless", False, "Download in lossy format if available (default)"),
])
def test_parse_arguments_help(option, value, help_text, capsys, monkeypatch):
    """Ensure the lossless config option correctly modifies the help text."""
    config = DEFAULTS.copy() # So that we don't modify the constant
    config[option] = value

    # Prevent argparse from hard-exiting
    def inhibit_exit(self, status=0, message=None):
        pass
    monkeypatch.setattr("argparse.ArgumentParser.exit", inhibit_exit)

    # Produce help message and look for the relevant lines
    parse_arguments(["/usr/bin/self", "--help"], config)
    stdout, stderr = capsys.readouterr()
    assert help_text in stdout
