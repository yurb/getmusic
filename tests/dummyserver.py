#!/usr/bin/env python3

"""A simple http server for use as an http proxy for testing purposes.

It servers files from a subdirectory in current working directory,
named after the host part of the url. So for example request for
http://archive.org/details/test will be responded with the contents of
the following file:

- current working directory
 - archive.org/
  - details/
   - test

"""
import http.server
import os
from sys import argv


def proxy_handler(root):
    class handler(http.server.SimpleHTTPRequestHandler):
        def translate_path(self, path):
            _path = os.path.join(
                root,
                path.replace('http://', '')
            )
            print(_path)
            return _path

    return handler


def server(root, port=0):
    server = http.server.HTTPServer(('', port),
                                    proxy_handler(root))
    return server


# For manual testing of the server
if __name__ == '__main__':
    try:
        root = argv[1]
    except KeyError:
        root = os.getcwd()

    _server = server(root, 8081)
    addr = _server.server_address
    print("Dummy server listening on {}:{}".format(*addr))
    _server.serve_forever()
