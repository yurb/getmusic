"""Tests for ArchiveOrgProvider"""

import pytest
from urllib.parse import urlparse
from getmusic.providers import ArchiveOrgProvider


@pytest.fixture
def ao():
    return ArchiveOrgProvider()


def test_item_id(ao):
    """Test the item id is correctly determined."""
    item = ao._get_item_id(urlparse("https://archive.org/details/rb001"))
    assert item == "rb001"


def test_item_id_fail(ao):
    """Ensure ArchiveOrgProvider.init raises ValueError for invalid URLs."""
    with pytest.raises(ValueError):
        ao._get_item_id(urlparse("https://web.archive.org/*/google.com"))


def test_info_url(ao):
    """Ensure the release info is fetched from the correct URL"""
    assert ao._get_info_url("rb001") == "https://archive.org/metadata/rb001"


@pytest.mark.parametrize("aorg_fmt, fmt", [
    ('VBR MP3', 'mp3'),
    ('256Kbps MP3', 'mp3'),
    ('Ogg Vorbis', 'ogg'),
    ('Flac', 'flac'),
    ('24bit Flac', 'flac'),
    ('AIFF', 'aiff'),
    ('WAVE', 'wav'),
    ('png', 'other')
])
def test_which_format(ao, aorg_fmt, fmt):
    assert ao._which_format(aorg_fmt) == fmt


def test_sorting(ao):
    """Test files are returned in correct sort order:
    - by track number, if present
    - then alphabetically by filename.
    """
    release_info = {
        'files': [
            {
                "name": "File 2",
                "format": "Ogg Vorbis",
            },
            {
                "name": "Track 2",
                "track": "2/2",
                "format": "Ogg Vorbis",
            },
            {
                "name": "Track 1",
                "track": "1/2",
                "format": "Ogg Vorbis",
            },
            {
                "name": "File 1",
                "format": "Ogg Vorbis",
            },
        ]
    }
    order = [asset.title for asset in ao._assets(release_info, 'testitem')]
    assert order == ["Track 1", "Track 2", "File 1", "File 2"]


@pytest.mark.parametrize('unquoted, quoted', [
    ("Test With Spaces.ogg", "Test%20With%20Spaces.ogg"),
    ("Test With & <> ?#\" //'.ogg",
     "Test%20With%20%26%20%3C%3E%20%3F%23%22%20%2F%2F%27.ogg"),
    ("Test With Юнікод.ogg",
     "Test%20With%20%D0%AE%D0%BD%D1%96%D0%BA%D0%BE%D0%B4.ogg")])
def test_url_quoted(ao, unquoted, quoted):
    prefix = "https://archive.org/download/testitem/"
    asset = ao._as_asset({
        "name": unquoted,
        "format": "Ogg Vorbis",
    }, "testitem")
    assert asset.url == "".join([prefix, quoted])
