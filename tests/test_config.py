import pytest
from getmusic.config import load_config, prioritize_formats, \
    UnsupportedPathPlaceholderError, MissingPathPlaceholderError

def create_temp_config(tmpdir, content):
    tmpfile = tmpdir.join("getmusic-test.conf")
    tmpfile.write(content)

    return tmpfile.strpath

def test_invalid_path_placeholder(tmpdir):
    config_file = create_temp_config(
        tmpdir,
        """[general]\nalbum_path = '{title}/{invalid_placeholder}'"""
    )

    with pytest.raises(UnsupportedPathPlaceholderError):
        load_config(config_file)

def test_mssing_path_placeholder(tmpdir):
    config_file = create_temp_config(
        tmpdir,
        """[general]\nalbum_path = '{artist}'""")

    with pytest.raises(MissingPathPlaceholderError):
        load_config(config_file)

def test_missing_required_conffile():
    with pytest.raises(ValueError):
        load_config("/tmp/nonexistent", required=True)

@pytest.mark.parametrize("formats_lossy,formats_lossless,lossless,result", [
    ('ogg,mp3', 'flac,wav', False, ['ogg', 'mp3', 'flac', 'wav']),
    ('mp3,ogg', 'wav,flac', False, ['mp3', 'ogg', 'wav', 'flac']),
    ('ogg,mp3', 'flac,wav', True, ['flac', 'wav', 'ogg', 'mp3']),
    ('mp3,ogg', 'wav,flac', True, ['wav', 'flac', 'mp3', 'ogg'])])
def test_prioritize_formats(formats_lossy, formats_lossless, lossless, result):
    config = {
        'formats_lossy': formats_lossy,
        'formats_lossless': formats_lossless,
        'lossless': lossless
    }
    assert prioritize_formats(config) == result
