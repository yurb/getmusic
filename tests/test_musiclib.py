import pytest
from getmusic.musiclib import MusicLibrary, Release, Asset, safe_filename

#
# Release
#
@pytest.mark.parametrize("formats, result", [
    (["ogg", "mp3", "flac"], Asset("Hello", "Hello.ogg", None, "ogg"))])
def test_find_best_media(formats, result):
    assets = [
        Asset("Hello", "Hello.ogg", None, "ogg"),
        Asset("Hello", "Hello.mp3", None, "mp3"),
        Asset("Hello", "Hello_vbr.mp3", None, "mp3"),
        Asset("Hello", "Hello_64kb.mp3", None, "mp3lofi"),
        Asset("Hello", "Hello.flac", None, "flac")
    ]
    release = Release({}, assets)
    assert release._find_best_media("Hello", formats) == result

def test_find_best_media_fail():
    """Ensure find_best_media raises the appropriate exception with
    the appropriate message."""
    assets = [
        Asset("Hello", "Hello.mp3", None, "mp3")
    ]
    release = Release({}, assets)
    with pytest.raises(RuntimeError) as exception:
        release._find_best_media("Hello", ["ogg", "flac"])
    assert str(exception.value) == \
        "Media 'Hello' is not available in any of the requested formats. " \
        "The requested formats include, in that order: ogg, flac."

@pytest.mark.parametrize("formats, result", [
    ("ogg,mp3,flac,wav".split(','), [
        Asset("Alpha", "Alpha.ogg", None, "ogg"),
        Asset("Beta", "Beta.ogg", None, "ogg"),
    ])])
def test_filter_assets(formats, result):
    assets = [
        Asset("Alpha", "Alpha.ogg", None, "ogg"),
        Asset("Alpha", "Alpha.mp3", None, "mp3"),
        Asset("Alpha", "Alpha.flac", None, "flac"),
        Asset("Beta", "Beta.ogg", None, "ogg"),
        Asset("Beta", "Beta.mp3", None, "mp3"),
        Asset("Beta", "Beta.flac", None, "flac")
    ]
    release = Release({}, assets)
    # Order is not important at the moment
    assert sorted(release.filter_assets(formats)) == sorted(result)

#
# Paths
#
@pytest.mark.parametrize("album_path_format, collection, artist, title, result", [
    ("{collection}/{artist} - {title}", "Archaic Horizon", "Gemini Tri", "Mirror",
     "Archaic Horizon/Gemini Tri - Mirror"),
    ("{collection}/{artist} - {title}", "conv", "Carlo Giordani / Rinus van Alebeek",
     "split [cnv36]",
     "conv/Carlo Giordani & Rinus van Alebeek - split cnv36")])
def test_get_album_path(album_path_format, collection, artist, title, result):
    config = {
        'album_path': album_path_format
    }
    meta = {
        'collection': collection,
        'artist': artist,
        'title': title
    }
    lib = MusicLibrary(config)
    assert lib._get_album_path(meta) == result

@pytest.mark.parametrize("unsafe, safe", [
    ("filename!?", "filename"),
    (r"\testTe\\st", "testTest"),
    ("привіт!/test", "привіт&test")
])
def test_safe_filename(unsafe, safe):
    assert safe_filename(unsafe) == safe

@pytest.mark.parametrize("overwrite", (False, True))
def test_musiclib_open_overwrite(wipetmpdir, overwrite):
    """Ensure we overwrite files only if asked with the ``overwrite`` parameter."""
    asset = Asset("AlreadyExists", "AlreadyExists.ogg", "url.irrelevant", "ogg")
    
    config = {
        "music_dir": str(wipetmpdir),
        "album_path": "test_album",
        "overwrite": overwrite
    }
    
    # Prepare test local file
    testfile = wipetmpdir.mkdir('test_album').join("AlreadyExists.ogg")
    testfile.write(b"not touched")

    lib = MusicLibrary(config)
    
    if overwrite:
        with lib.open(asset, {}) as target:
            target.write(b"touched")
        assert testfile.read_binary() == b"touched"
    else:
        with pytest.raises(FileExistsError):
            with lib.open(asset, {}) as target:
                target.write(b"touched")
