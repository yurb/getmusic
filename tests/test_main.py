"""The goal here is to test that the main function works and that the
program works correctly as a whole.

We'll create a dummy http server that will serve some test data to
us. We will then try to download it and verify it was correctly
downloaded.
"""
import pytest
import os

from threading import Thread

from dummyserver import server

DUMMY_SERVER_DIR = os.path.join(
    str(pytest.config.rootdir),
    'tests/dummyserver_data')

TEST_RELEASE = {
    'url': 'http://archive.org/details/testrelease',
    'files': [
        {'src': 'archive.org/download/testrelease/01_astral_recorder.ogg',
         'dest': 'archaichorizon/GABRIEL/Good Old Days/01_astral_recorder.ogg'},
        {'src': 'archive.org/download/testrelease/02_fun_go_round.ogg',
         'dest': 'archaichorizon/GABRIEL/Good Old Days/02_fun_go_round.ogg'},
    ],
}


@pytest.fixture(scope='module')
def dummyserver():
    _server = server(DUMMY_SERVER_DIR)
    _server_thread = Thread(target=_server.serve_forever)
    _server_thread.daemon = True
    _server_thread.start()

    yield _server

    _server.shutdown()


def test_main(dummyserver, script_runner, tmpdir):
    """Run getmusic against our dummy server.

    After running, verify files downloaded correctly.
    """
    # Prepare config
    config = tmpdir.join('getmusic.conf')
    config.write("[general]\n"
                 "music_dir: {}".format(str(tmpdir)))
    os.environ["HTTP_PROXY"] = "http://{}:{}".format(
        *dummyserver.server_address
    )
    result = script_runner.run('getmusic',
                               '--config={}'.format(str(config)),
                               TEST_RELEASE['url'])

    for download in TEST_RELEASE['files']:
        src_path = os.path.join(DUMMY_SERVER_DIR, download['src'])
        dest_path = str(tmpdir.join(download['dest']))
        with open(src_path, 'rb') as _src:
            src = _src.read()
        with open(dest_path, 'rb') as _dest:
            dest = _dest.read()

    assert result.success and (src == dest)
