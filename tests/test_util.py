import pytest
from getmusic.util import error, unique

@pytest.mark.parametrize("exception", [RuntimeError, ValueError])
def test_with_error(exception):
    with pytest.raises(exception) as e_instance:
        with error("Test message", exception):
            raise exception("Houston, we have a problem")
    assert str(e_instance.value) == "Test message: Houston, we have a problem"

@pytest.mark.parametrize("collection, result", [
    (["one", "two", "two", "three"], ["one", "two", "three"]),
    (["one", 2, 2, "three"], ["one", 2, "three"])
])
def test_unique(collection, result):
    assert unique(collection) == result
