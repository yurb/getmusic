"""Useful utilities shared among tests"""
import pytest

#
# Fixtures
#
@pytest.fixture
def wipetmpdir(tmpdir):
    """A tmpdir that will delete itself after the test"""
    yield tmpdir
    tmpdir.remove()
