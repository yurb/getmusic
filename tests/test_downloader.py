import os
import pytest
from unittest.mock import MagicMock

import getmusic.downloader as d
import getmusic.network as network
from getmusic.musiclib import Asset, Release

#
# Fixtures
#
@pytest.fixture
def wipemusiclib(wipetmpdir):
    """A music lib that writes to a wipetmpdir"""
    lib = MagicMock()
    lib.open = lambda Asset, meta: open(str(wipetmpdir.join(Asset.filename)), 'xb')
    return lib

#
# Entry points
#
def test_list_media():
    provider = MagicMock()
    provider.get_release.return_value = Release({}, [
        Asset("Alpha", "Alpha.ogg", "https://some.url/media/Alpha.ogg", "ogg"),
        Asset("Beta", "Beta.ogg", "https://some.url/media/Beta.ogg", "ogg")
    ])
    assert d.list_media("dummyurl", provider, ["ogg"]) == [
        "https://some.url/media/Alpha.ogg",
        "https://some.url/media/Beta.ogg"
    ]

#
# Network & disk IO
#
def test_download_assets(wipetmpdir, wipemusiclib, monkeypatch):
    assets = [
        Asset("Alpha", "Alpha.ogg", "https://some.url/media/Alpha.ogg", "ogg"),
        Asset("Beta", "Beta.ogg", "https://some.url/media/Beta.ogg", "ogg")
    ]

    # A kilobyte of random data to "download" and then compare to
    data = os.urandom(1024)

    # Mock network.get_to_file
    def fake_get_to_file(url, dest_file):
        dest_file.write(data)
    monkeypatch.setattr(network, 'get_to_file', fake_get_to_file)

    # Perform
    d.download_assets(Release({}, assets), wipemusiclib, ["ogg"])

    # Verify
    for filename in ("Alpha.ogg", "Beta.ogg"):
        assert wipetmpdir.join(filename).read_binary() == data
