import pytest
from getmusic.providers import KahviProvider
from getmusic.musiclib import Asset
import getmusic.network as network

@pytest.fixture
def kahvi():
    return KahviProvider()

@pytest.mark.parametrize("url, result", [
    ("http://kahvi.org/release.php?release_number=353", "353"),
    ("http://kahvi.org/playlist.php?release_number=353", "353")])
def test_release_id(url, result):
    p = KahviProvider()
    release_id = p._get_release_id(url)
    assert release_id == result

def test_metadata_network_fail(monkeypatch, kahvi):
    def network_get_fail(url=None):
        raise RuntimeError("Network error")
    monkeypatch.setattr(network, 'get', network_get_fail)

    with pytest.raises(RuntimeError) as exception:
        kahvi._metadata("http://kahvi.org/release.php?release_number=000")
    assert str(exception.value) == "Failed to fetch the release page: Network error"

def test_metadata(monkeypatch, kahvi):
    html = """random
    <title>#372: Faex Optim / Start with the River - Kahvi Collective Electronic Vibes</title>
    random"""
    metadata = {
        'artist': "Faex Optim",
        'title': "Start with the River",
        'collection': "kahvi",
        'license': "http://creativecommons.org/licenses/by-nc-nd/3.0/"
    }
    monkeypatch.setattr(network, 'get', lambda url: html)
    assert kahvi._metadata("dummyurl") == metadata

def test_assets(monkeypatch, kahvi):
    xml = """
    <playlist version='1' xmlns='http://xspf.org/ns/0/'>
    <title>Kahvi Collective</title>
    <tracklist>
    <track>

    <creator>weedie </creator>
    <title>ego cogito cogitatum</title>
    <album>kahvi</album>
    <image>http://www.kahvi.org/stream.jpg</image>
    <location>http://kahvi.micksam7.com/mp3/kahvi349b_weedie-ego_cogito_cogitatum.mp3</location>
    <info>http://www.kahvi.org/releases.php?release_number=349</info>
    </track>

    <track>
    <creator>weedie </creator>
    <title>eik ir motyvuokis</title>
    <album>kahvi</album>
    <image>http://www.kahvi.org/stream.jpg</image>
    <location>http://kahvi.micksam7.com/mp3/kahvi349c_weedie-eik_ir_motyvuokis.mp3</location>
    <info>http://www.kahvi.org/releases.php?release_number=349</info>
    </track>

    </tracklist>
    </playlist>
    """
    assets = [
        Asset(title='ego cogito cogitatum',
              filename='kahvi349b_weedie-ego_cogito_cogitatum.mp3',
              url='http://kahvi.micksam7.com/mp3/kahvi349b_weedie-ego_cogito_cogitatum.mp3',
              fmt='mp3'),
        Asset(title='eik ir motyvuokis',
              filename='kahvi349c_weedie-eik_ir_motyvuokis.mp3',
              url='http://kahvi.micksam7.com/mp3/kahvi349c_weedie-eik_ir_motyvuokis.mp3',
              fmt='mp3')
    ]
    monkeypatch.setattr(network, 'get', lambda url: xml)
    assert kahvi._assets("dummyurl") == assets
